Name:          biosdevname
Version:       0.7.3
Release:       4
Summary:       A Udev tool give BIOS-given name for device
License:       GPLv2
URL:           http://linux.dell.com/files/biosdevname
Source0:       https://github.com/dell/biosdevname/archive/v0.7.3.tar.gz

ExclusiveArch: x86_64
BuildRequires: autoconf automake pciutils-devel zlib-devel gcc

%description
In its simplest form, biosdevname takes a kernel device name as an argument, gives the BIOS-given
name it "should" be. This is necessary on systems where the BIOS name for a given device (e.g.
labeled "Gb1" on the chassis) doesn't map directly and obviously to the kernel name (e.g. eth0).

%package       help
Summary:       Help documents for biosdevname

%description   help
This package contains help documents for biosdevname

%prep
%autosetup -p1 -n biosdevname-0.7.3

%build
autoreconf -fvi
%configure --prefix=%{_prefix}
%make_build

%install
%make_install install-data

%files
%doc COPYING README
%{_sbindir}/%{name}
/lib/udev/rules.d/*.rules

%files help
%{_mandir}/man1/%{name}.1*

%changelog
* Mon May 31 2021 baizhonggui <baizhonggui@huawei.com> - 0.7.3-4
- Fix configure: error: no acceptable C compiler found in $PATH
- Add gcc in BuildRequires

* Tue Mar 3 2020 Chen Dingxiao <chendingxiao1@huawei.com> - 0.7.3-3
- Package init
